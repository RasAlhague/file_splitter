//! Contains the implementations for xaml file spliting.
extern crate regex;

use crate::code_file::CodeFile;
use crate::code_file::get_regex_val;
use regex::Regex;

/// Represents a XamlFile
pub struct XamlFile {
    name: String,
    content: String,
}

impl CodeFile for XamlFile {
    /// Implementation for `extension` of `CodeFile`. Returns always `.xaml`
    fn extension(&self) -> &str {
        ".xaml"
    }
    /// Implementation for `name` of `CodeFile`. Returns the name of the file
    fn name(&self) -> &str {
        &self.name
    }
    /// Implementation for `content` of `CodeFile`. Returns the content of the file.
    fn content(&self) -> &str {
        &self.content
    }
}

impl XamlFile {
    /// Constructs a `XamlFile` from the given content and returns it.
    ///
    /// # Arguments
    ///
    /// * `content` - a `&str` which contains the content from which it should be created.
    ///
    /// # Errors
    ///
    /// Returns an Error of type &'static str if regex didn't match.
    pub fn create(content: &str) -> Result<XamlFile, &'static str> {
        let re: Regex = Regex::new("x:class=\"[a-zA-Z0-9.]+\"").unwrap();

        let mut name: String = get_regex_val(re, content);

		let split = name.split('"');
		let vec: Vec<&str> = split.collect();

		let substring = String::from(vec[vec.len() - 2]);
		let vec: Vec<&str> = substring.split('.').collect();

		name = String::from(vec[vec.len() - 1]);

        if name == "" {
            return Err("The regex didn't match for a name or the name was empty");
        }

        Ok(XamlFile {
            name,
            content: String::from(content),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create() {
        let test_value = "x:class=\"Test.TestClass\">";

        let result = XamlFile::create(test_value).unwrap();
        assert_eq!("TestClass", result.name());
    }

    #[test]
    fn test_extension() {
        let public_class = "x:class=\"Test.TestClass\">";

        let result = XamlFile::create(public_class).unwrap();
        assert_eq!(".xaml", result.extension());
    }

    #[test]
    fn test_name() {
        let public_class = "x:class=\"Test.TestClass\">";

        let result = XamlFile::create(public_class).unwrap();
        assert_eq!("TestClass", result.name());
    }

    #[test]
    fn test_content() {
        let public_class = "x:class=\"Test.TestClass\"";

        let result = XamlFile::create(public_class).unwrap();
        assert_eq!("x:class=\"Test.TestClass\"", result.content());
    }
}
