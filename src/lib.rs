// lib.rs lib
extern crate regex;
mod code_file;
pub mod config;
mod csharp_file;
mod rust_file;
mod xaml_file;

use crate::code_file::code_file_factory;
pub use crate::config::Config;
use std::fs;

/// Represents the possible languages which files can be split
pub enum Language {
    CSharp,
    Rust,
    Xaml,
}

/// Runs the processing code
pub fn run(config: Config) {
    println!(
        "Config -> Input: {}, Output: {}",
        config.input_file_path, config.output_file_path
    );
    let contents: String =
        fs::read_to_string(config.input_file_path).expect("input file doesnt exists");

    let split = contents.split(&config.seperator);

    for content in split {
        let file = match code_file_factory(&config.language, &content) {
            Ok(val) => val,
            Err(_) => {
                println!("Couldn't create file from content");
                continue;
            }
        };

        match file.write(&config.output_file_path) {
            Ok(_) => println!(
                "File succesfully written to {}{}{}",
                config.output_file_path,
                file.name(),
                file.extension()
            ),
            Err(_) => {
                println!(
                    "File couldn't be written to {}{}{}",
                    config.output_file_path,
                    file.name(),
                    file.extension()
                );
                continue;
            }
        }
    }

    println!("Finished processing and writing...");
}
