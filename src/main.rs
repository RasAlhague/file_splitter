// main.rs main
use std::process;

const VERSION: &str = env!("CARGO_PKG_VERSION");

/// Main function, entry point.
fn main() {
    println!("{}", get_version());
    let config = file_splitter::Config::new();

    file_splitter::run(config);
    pause();
}

/// returns the current version as a `String`.
fn get_version() -> String {
    format!("file_splitter Version: {}\n", VERSION)
}

/// Pauses the program in C-style.
///
/// # Remarks
///
/// Uses windows commands so not crossplatform compatible.
fn pause() {
    let _ = process::Command::new("cmd.exe")
        .arg("/c")
        .arg("pause")
        .status();
}
