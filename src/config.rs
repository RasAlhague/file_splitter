extern crate structopt;

use crate::Language;
use structopt::StructOpt;

/// Contains the configuration of the program read from the environment args.
#[derive(StructOpt)]
#[structopt(
    name = "file_splitter",
    about = "Program to split code files from a single big one"
)]
pub struct Config {
    /// Path and filename of the file which should be split into smaller ones
    #[structopt(short = "i", long = "input_file_path")]
    pub input_file_path: String,
    /// Path where the outputted files are written to
    #[structopt(short = "o", long = "output_file_path")]
    pub output_file_path: String,
    /// String, which is used to seperate the files. "---" is not allowed.
    #[structopt(short = "s", long = "seperator")]
    pub seperator: String,
    /// Programming language of the files.
    #[structopt(short = "l", long = "language", parse(try_from_str = "parse_language"))]
    pub language: Language,
}

/// Takes a string reference and creates an Language struct from it.
///
/// # Supported Params
///
/// * `Rust`, `rust`, `rs` for rust files
/// * `Xaml`, `xaml` for Xaml files
/// * `CSharp`, `cs`, `CS`, `csharp`, `C#` for C# Files.
///
/// # Arguments
///
/// * `lang` - a `&str` which contains the language of the file.
///
/// # Errors
///
/// Returns an Error of type &'static str if the Lang isnt supported.
fn parse_language(lang: &str) -> Result<Language, &'static str> {
    match lang {
        "Rust" | "rust" | "rs" => Ok(Language::Rust),
        "Xaml" | "xaml" => Ok(Language::Xaml),
        "CSharp" | "CS" | "cs" | "csharp" | "C#" => Ok(Language::CSharp),
        _ => Err("Invalid parameter"),
    }
}

impl Config {
    /// Creates a new Config instance from environment args
    pub fn new() -> Config {
        Config::from_args()
    }
}

impl Default for Config {
    // Dont Remember why i need this, but yeah maybe i can kill it later xD
    fn default() -> Self {
        Self::new()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_language_csharp() {
        let test_value = "C#";

        let result = parse_language(test_value).unwrap();

        let end_result = match result {
            Language::CSharp => true,
            _ => false,
        };

        assert!(end_result);
    }

    #[test]
    fn test_parse_language_rust() {
        let test_value = "rs";

        let result = parse_language(test_value).unwrap();

        let end_result = match result {
            Language::Rust => true,
            _ => false,
        };

        assert!(end_result);
    }

    #[test]
    fn test_parse_language_xaml() {
        let test_value = "Xaml";

        let result = parse_language(test_value).unwrap();

        let end_result = match result {
            Language::Xaml => true,
            _ => false,
        };

        assert!(end_result);
    }
}