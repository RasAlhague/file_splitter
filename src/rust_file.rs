//! Contains the implementations for rust file spliting.
extern crate regex;

use crate::code_file::CodeFile;
use crate::code_file::get_regex_val;
use regex::Regex;

/// Represents a Rust file.
pub struct RustFile {
    name: String,
    content: String,
}

impl CodeFile for RustFile {
    /// Implementation for `extension` of `CodeFile`. Returns always `.rs`
    fn extension(&self) -> &str {
        ".rs"
    }
    /// Implementation for `name` of `CodeFile`. Returns the name of the file
    fn name(&self) -> &str {
        &self.name
    }
    /// Implementation for `content` of `CodeFile`. Returns the content of the file.
    fn content(&self) -> &str {
        &self.content
    }
}

impl RustFile {
    /// Constructs a `RustFile` from the given content and returns it.
    ///
    /// # Arguments
    ///
    /// * `content` - a `&str` which contains the content from which it should be created.
    ///
    /// # Errors
    ///
    /// Returns an Error of type &'static str if regex didn't match.
    pub fn create(content: &str) -> Result<RustFile, &'static str> {
        let re: Regex = Regex::new("// [a-zA-Z0-9_]+.rs [a-zA-Z0-9_]+").unwrap();

        let mut name: String = get_regex_val(re, content);
		
		let split = name.split(' ');
		let vec: Vec<&str> = split.collect();
		name = String::from(vec[vec.len() - 1]);

        if name == "" {
            return Err("The regex didn't match for a name or the name was empty");
        }

        Ok(RustFile {
            name,
            content: String::from(content),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create() {
        let test_value = "// main.rs main";

        let result = RustFile::create(test_value).unwrap();
        assert_eq!("main", result.name());
    }

    #[test]
    fn test_extension() {
        let public_class = "// main.rs main";

        let result = RustFile::create(public_class).unwrap();
        assert_eq!(".rs", result.extension());
    }

    #[test]
    fn test_name() {
        let public_class = "// main.rs main";

        let result = RustFile::create(public_class).unwrap();
        assert_eq!("main", result.name());
    }

    #[test]
    fn test_content() {
        let public_class = "// main.rs main";

        let result = RustFile::create(public_class).unwrap();
        assert_eq!("// main.rs main", result.content());
    }
}
