///! Contains the abstract base and the creation methods for codefiles.
extern crate regex;

use crate::csharp_file::CSharpFile;
use crate::rust_file::RustFile;
use crate::xaml_file::XamlFile;
use regex::Regex;
use crate::Language;
use std::fs::File;
use std::io::prelude::*;

/// Trait which abstracts methods for codefiles.
pub trait CodeFile {
    /// Should return the file extension of the codefile.
    fn extension(&self) -> &str;
    /// Should return the name of the codefile.
    fn name(&self) -> &str;
    /// Should return the content of the codefile.
    fn content(&self) -> &str;

    /// Writes the content of the codefile to the given location.
    ///
    /// # Arguments
    ///
    /// * `directory` - a `&str` which contains the directory where to write to.
    ///
    /// # Errors
    ///
    /// Returns an Error of type &'static str if file could not be writen.
    ///
    /// # Remarks
    ///
    /// Has standard implementiation.
    fn write(&self, directory: &str) -> Result<(), &'static str> {
        let file_path = format!("{}{}{}", directory, self.name(), self.extension());

        let mut file = match File::create(&file_path) {
            Ok(file) => file,
            Err(_) => return Err("Couldn't create file"),
        };

        // returns nothing if successfull
        match file.write_all(self.content().as_bytes()) {
            Ok(_) => Ok(()),
            Err(_) => Err("Couldn't write file"),
        }
    }
}

/// Returns a `CodeFile` trait-object from a given language and content.
///
/// # Arguments
///
/// * `content` - a `&str` which contains the content of the codefile.
/// * `language` - a `&Language` reference which determines the trait-object type.
///
/// # Errors
///
/// Returns an Error of type &'static str if trait object could not be created.
pub fn code_file_factory(
    language: &Language,
    content: &str,
) -> Result<Box<dyn CodeFile>, &'static str> {
    // tries to create the right CodeFile object.
    let file: Box<dyn CodeFile> = match language {
        Language::CSharp => {
            let cs_file = match CSharpFile::create(content) {
                Ok(val) => val,
                Err(_) => return Err("Couldn't create CSharpFile"),
            };

            Box::new(cs_file)
        }
        Language::Rust => {
            let rs_file = match RustFile::create(content) {
                Ok(val) => val,
                Err(_) => return Err("Couldn't create RustFile"),
            };

            Box::new(rs_file)
        }
        Language::Xaml => {
            let xaml_file = match XamlFile::create(content) {
                Ok(val) => val,
                Err(_) => return Err("Couldn't create XamlFile"),
            };

            Box::new(xaml_file)
        }
    };

    Ok(file)
}

pub fn get_regex_val(re: Regex, content: &str) -> String {
	match re.find(content) {
		Some(re_match) => String::from(&content[re_match.start() .. re_match.end()]),
		None    => String::from(""),
	}
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_code_file_factory_csharp() {
        let public_class = "public class TestA {}";
        let public_inteface = "public interface ITestA {}";
        let public_struct = "public struct TestA {}";
        let internal_enum = "public enum TestA {}";

        let language = Language::CSharp;

        let mut result = code_file_factory(&language, public_class).unwrap();
        assert_eq!("TestA", result.name());

        result = code_file_factory(&language, public_inteface).unwrap();
        assert_eq!("ITestA", result.name());

        result = code_file_factory(&language, public_struct).unwrap();
        assert_eq!("TestA", result.name());

        result = code_file_factory(&language, internal_enum).unwrap();
        assert_eq!("TestA", result.name());
    }

    #[test]
    fn test_code_file_factory_rust() {
        let test_value = "// main.rs main";

        let language = Language::Rust;

        let result = code_file_factory(&language, test_value).unwrap();
        assert_eq!("main", result.name());
    }

    #[test]
    fn test_code_file_factory_xaml() {
        let test_value = "x:class=\"Test.TestClass\">";

        let language = Language::Xaml;

        let result = code_file_factory(&language, test_value).unwrap();
        assert_eq!("TestClass", result.name());
    }

    #[test]
    fn test_write_csharp() {
        let public_class = "public class TestA {}";
        let language = Language::CSharp;

        let result = code_file_factory(&language, public_class).unwrap();
        let write_res = result.write("./").unwrap();

        assert_eq!((), write_res);
    }

    #[test]
    fn test_write_rust() {
        let public_class = "// main.rs main";
        let language = Language::Rust;

        let result = code_file_factory(&language, public_class).unwrap();
        let write_res = result.write("./").unwrap();

        assert_eq!((), write_res);
    }

    #[test]
    fn test_write_xaml() {
        let public_class = "x:class=\"Test.TestClass\">";
        let language = Language::Xaml;

        let result = code_file_factory(&language, public_class).unwrap();
        let write_res = result.write("./").unwrap();

        assert_eq!((), write_res);
    }
}