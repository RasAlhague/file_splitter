//! Contains the implementation details for splitting of C# files.
extern crate regex;

use crate::code_file::CodeFile;
use crate::code_file::get_regex_val;
use regex::Regex;

/// Struct which represents a C# file.
pub struct CSharpFile {
    name: String,
    content: String,
}

impl CodeFile for CSharpFile {
    /// Implementation for `extension` of `CodeFile`. Returns always `.cs`
    fn extension(&self) -> &str {
        ".cs"
    }
    /// Implementation for `name` of `CodeFile`. Returns the name of an `CSharpFile`
    fn name(&self) -> &str {
        &self.name
    }
    /// Implementation for `content` of `CodeFile`. Returns the content of the file.
    fn content(&self) -> &str {
        &self.content
    }
}

impl CSharpFile {
    /// Constructs a `CSharpFile` from the given content and returns it.
    ///
    /// # Arguments
    ///
    /// * `content` - a `&str` which contains the content from which it should be created.
    ///
    /// # Errors
    ///
    /// Returns an Error of type &'static str if regex didn't match.
    pub fn create(content: &str) -> Result<CSharpFile, &'static str> {
        // Builds the regex. I would rather like to build the regex 1 time static,
        // instead of every time but for time being must this do.
        let re: Regex = Regex::new(
            "(public|internal|protected)[a-zA-Z\\s]+(class|enum|struct|interface) [a-zA-Z]+",
        )
        .unwrap();

        let mut name: String = get_regex_val(re, content);
        let split = name.split(' ');
        let vec: Vec<&str> = split.collect();
        name = String::from(vec[vec.len() - 1]);

        if name == "" {
            return Err("The regex didn't match for a name or the name was empty");
        }

        Ok(CSharpFile {
            name,
            content: String::from(content),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create() {
        let public_class = "public class TestA {}";
        let public_inteface = "public interface ITestA {}";
        let public_struct = "public struct TestA {}";
        let internal_enum = "public enum TestA {}";

        let mut result = CSharpFile::create(public_class).unwrap();
        assert_eq!("TestA", result.name());

        result = CSharpFile::create(public_inteface).unwrap();
        assert_eq!("ITestA", result.name());

        result = CSharpFile::create(public_struct).unwrap();
        assert_eq!("TestA", result.name());

        result = CSharpFile::create(internal_enum).unwrap();
        assert_eq!("TestA", result.name());
    }

    #[test]
    fn test_extension() {
        let public_class = "public class TestA {}";

        let result = CSharpFile::create(public_class).unwrap();
        assert_eq!(".cs", result.extension());
    }

    #[test]
    fn test_name() {
        let public_class = "public class TestA {}";

        let result = CSharpFile::create(public_class).unwrap();
        assert_eq!("TestA", result.name());
    }

    #[test]
    fn test_content() {
        let public_class = "public class TestA {}";

        let result = CSharpFile::create(public_class).unwrap();
        assert_eq!("public class TestA {}", result.content());
    }
}